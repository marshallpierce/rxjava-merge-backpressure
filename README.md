Run `ExperimentMain` (or use `./gradlew run`) and you'll see output like this:

```
(a few timestamps...)
rx.exceptions.MissingBackpressureException
	at rx.internal.util.RxRingBuffer.onNext(RxRingBuffer.java:349)
	at rx.internal.operators.OperatorMerge$InnerSubscriber.enqueue(OperatorMerge.java:721)
	at rx.internal.operators.OperatorMerge$InnerSubscriber.emit(OperatorMerge.java:698)
	at rx.internal.operators.OperatorMerge$InnerSubscriber.onNext(OperatorMerge.java:586)
	at rx.subjects.SubjectSubscriptionManager$SubjectObserver.onNext(SubjectSubscriptionManager.java:224)
	at rx.subjects.PublishSubject.onNext(PublishSubject.java:114)
	at org.mpierce.rxjava.merge.ExperimentMain.lambda$main$0(ExperimentMain.java:28)
	at org.mpierce.rxjava.merge.ExperimentMain$$Lambda$1/1685538367.call(Unknown Source)
	at rx.Scheduler$Worker$1.call(Scheduler.java:120)
	at rx.internal.schedulers.ScheduledAction.run(ScheduledAction.java:55)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$201(ScheduledThreadPoolExecutor.java:180)
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:293)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
```
