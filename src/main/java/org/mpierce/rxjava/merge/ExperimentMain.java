package org.mpierce.rxjava.merge;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicBoolean;
import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static rx.Observable.merge;

final class ExperimentMain {
    public static void main(String[] args) throws InterruptedException {

        // Set small buffer size to make error happen faster
        System.setProperty("rx.ring-buffer.size", "10");

        Scheduler scheduler = Schedulers.newThread();

        PublishSubject<Long> occasional = PublishSubject.create();

        Scheduler.Worker worker = scheduler.createWorker();
        worker.schedulePeriodically(() -> occasional.onNext(1L), 0, 200, MILLISECONDS);

        Observable<Long> merged = merge(occasional, Observable.never());
        Observable<Long> throttled = merged.throttleWithTimeout(10, MINUTES);

        AtomicBoolean done = new AtomicBoolean(false);

        throttled.observeOn(scheduler).subscribe((i) -> System.out.println("Got " + i), (t) -> {
            t.printStackTrace();
            System.err.println(LocalDateTime.now());
            done.set(true);
        });

        while (!done.get()) {
            System.out.println(LocalDateTime.now());
            Thread.sleep(1000);
        }
    }
}
