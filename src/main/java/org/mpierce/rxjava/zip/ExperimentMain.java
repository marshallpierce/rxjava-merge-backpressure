package org.mpierce.rxjava.zip;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import rx.Observable;
import rx.schedulers.Schedulers;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

final class ExperimentMain {
    public static void main(String[] args) throws InterruptedException {

        // Set small buffer size to make error happen faster
        System.setProperty("rx.ring-buffer.size", "10");

        Observable<List<Object>> buffer1 = Observable.never().buffer(1, MILLISECONDS);
        Observable<List<Object>> buffer2 = Observable.never().buffer(1000, MILLISECONDS);

        Observable<Void> zipped = Observable.zip(buffer1, buffer2, (objects, objects2) -> null);

        AtomicBoolean done = new AtomicBoolean(false);

        zipped.observeOn(Schedulers.newThread()).subscribe((i) -> System.out.println("Got " + i), (t) -> {
            t.printStackTrace();
            System.err.println(LocalDateTime.now());
            done.set(true);
        });

        while (!done.get()) {
            System.out.println(LocalDateTime.now());
            Thread.sleep(1000);
        }
    }
}
